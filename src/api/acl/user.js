import axios from '../../utils/request'

const api_name = '/admin/acl/user'

/* 
获取当前用户的菜单权限列表
*/
export const getMenu = () => {
  return axios('/admin/acl/index/menu')
}


/* 
获取后台用户分页列表(带搜索)
*/
export const getPageList = (page, limit, username) =>{
  return axios({
    url: `${api_name}/${page}/${limit}`,
    method: 'get',
    params:{username:username || undefined}
  })
}

/* 
根据ID获取某个后台用户
*/
export const getById = (id) =>{
  return axios({
    url: `${api_name}/get/${id}`,
    method: 'get'
  })
}

/* 
保存一个新的后台用户
*/
export const add = (user) => {
  return axios({
    url: `${api_name}/save`,
    method: 'post',
    data: user
  })
}

/* 
更新一个后台用户
*/
export const update = (user) => {
  return axios({
    url: `${api_name}/update`,
    method: 'put',
    data: user
  })
}

/* 
获取某个用户的所有角色
*/
export const getRoles = (userId) => {
  return axios({
    url: `${api_name}/toAssign/${userId}`,
    method: 'get'
  })
}

/* 
给某个用户分配角色
roleId的结构: 字符串, 'rId1,rId2,rId3'
*/
export const assignRoles = (userId, roleId) =>{
  return axios({
    url: `${api_name}/doAssign`,
    method: 'post',
    params: {
      userId,
      roleId
    }
  })
}

/* 
删除某个用户
*/
export const removeById = (id) =>{
  return axios({
    url: `${api_name}/remove/${id}`,
    method: 'delete'
  })
}

/* 
批量删除多个用户
ids的结构: ids是包含n个id的数组
*/
export const removeUsers = (ids) => {
  return axios({
    url: `${api_name}/batchRemove`,
    method: 'delete',
    data: ids
  })
}