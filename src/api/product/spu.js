import axios from '../../utils/request'

//获取spu列表数据
export const reqSpuInfoList = (page,limit,category3Id) =>{
    return axios({
        url:`/admin/product/${page}/${limit}`,
        method:'get',
        params:{category3Id}
    })
}

//获取spu编辑页面spu名、spu描述
export const reqSpuNameDescList = (spuId) =>{
    return axios({
        url:`/admin/product/getSpuById/${spuId}`,
        method:'get',
    })
}

//获取spu编辑页面品牌
export const reqSpuTrademarkList = () =>{
    return axios({
        url:'/admin/product/baseTrademark/getTrademarkList',
        method:'get',
    })
}

//获取spu编辑页面图片
export const reqSpuImageList = (spuId) =>{
    return axios({
        url:`/admin/product/spuImageList/${spuId}`,
        method:'get',
    })
}

//获取spu编辑页面销售属性
export const reqSaleAttrList = () =>{
    return axios({
        url:'/admin/product/baseSaleAttrList',
        method:'get',
    })
}

//新增spu
export const reqAddSpu = (spuInfo)=>{
    return axios({
        url:'/admin/product/saveSpuInfo',
        method:'post',
        data:spuInfo
    })
}

//修改spu
export const reqUpdateSpu = (spuInfo)=>{
    return axios({
        url:'/admin/product/updateSpuInfo',
        method:'post',
        data:spuInfo
    })
}

//删除spu
export const reqDeleteSpu = (spuId)=>{
    return axios({
        url:`/admin/product/deleteSpu/${spuId}`,
        method:'delete',
    })
}


   
