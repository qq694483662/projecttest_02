import axios from '../../utils/request'

//获取品牌数据
export const reqTradeMark = (page,limit) => {
    return axios({   
        url:`/admin/product/baseTrademark/${page}/${limit}`,
        method:'get',
    })
}

//新增品牌
export const reqAddTradeMark = (tradeMark)=>{
    return axios({
        url:'/admin/product/baseTrademark/save',
        method:'post',
        data:tradeMark
    })
}

//修改品牌
export const reqUpdateTradeMark = (tradeMark)=>{
    return axios({
        url:'/admin/product/baseTrademark/update',
        method:'put',
        data:tradeMark
    })
}

//删除品牌
export const reqDeleteTradeMark = (id)=>{
    return axios({
        url:`/admin/product/baseTrademark/remove/${id}`,
        method:'delete',
    })
}
