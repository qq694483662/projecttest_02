import axios from '../../utils/request'

//获取spu图片
export const reqSpuImageList = (spuId)=>{
    return axios({
        url:`/admin/product/spuImageList/${spuId}`,
        method:'get'
    })
}

//获取销售属性
export const reqSpuSaleAttrList = (spuId)=>{
    return axios({
        url:`/admin/product/spuSaleAttrList/${spuId}`,
        method:'get'
    })
}


//获取属性列表
export const reqAttrInfoList = (category1Id,category2Id,category3Id) =>{
    return axios({
        url:`/admin/product/attrInfoList/${category1Id}/${category2Id}/${category3Id}`,
        method:'get'
    })
}

//打开sku添加页面,获取数据
export const reqAddSku = (skuInfo) =>{
    return axios({
        url:'/admin/product/saveSkuInfo',
        method:'post',
        data:skuInfo
    })
}

//获取sku弹框数据
export const reqSkuList = (spuId) =>{
    return axios({
        url:`/admin/product/findBySpuId/${spuId}`,
        method:'get'
    })
}



//获取sku管理页面数据
export const reqSkuInfoList = (page,limit) =>{
  return axios({
      url:`/admin/product/list/${page}/${limit}`,
      method:'get'
  })
}

//上架
export const reqOnSale = (skuId) =>{
    return axios({
        url:`/admin/product/onSale/${skuId}`,
        method:'get'
    })
}

//下架
export const reqOffSale = (skuId) =>{
    return axios({
        url:`/admin/product/cancelSale/${skuId}`,
        method:'get'
    })
}


//打开SKU弹窗,获取数据
export const reqSkuById = (skuId) =>{
    return axios({
        url:`/admin/product/getSkuById/${skuId}`,
        method:'get'
    })
}

//删除sku
export const reqDeleteSku = (skuId) =>{
    return axios({
        url:`/admin/product/deleteSku/${skuId}`,
        method:'delete'
    })
}



