import axios from '../../utils/request'

//获取分类1
export const reqCategory1List = () => {
    return axios({
        url:'/admin/product/getCategory1',
        method:'get'
    })
}

//获取分类2
export const reqCategory2List = (category1Id) => {
    return axios({
        url:`/admin/product/getCategory2/${category1Id}`,
        method:'get'
    })
}

//获取分类3
export const reqCategory3List = (category2Id) => {
    return axios({
        url:`/admin/product/getCategory3/${category2Id}`,
        method:'get'
    })
}

//获取属性列表
export const reqAttrInfoList = (category1Id,category2Id,category3Id) =>{
    return axios({
        url:`/admin/product/attrInfoList/${category1Id}/${category2Id}/${category3Id}`,
        method:'get'
    })
}

//添加属性
export const reqSaveAttrInfo=(attrInfo)=>{
  return axios({
      url:'/admin/product/saveAttrInfo',
      method:'post',
      data:attrInfo
  })
}

//删除属性
export const reqDeleteAttr=(attrId)=>{
  return axios({
      url:`/admin/product/deleteAttr/${attrId}`,
      method:'delete'
  })
}

//修改属性
export const reqUpdateAttrInfo=(attrInfo)=>{
  return axios({
      url:'/admin/product/saveAttrInfo',
      method:'post',
      data:attrInfo
  })
}

//获取属性值列表
export const reqAttrValueInfo=(attrId)=>{
  return axios({
      url:`/admin/product/getAttrValueList/${attrId}`,
      method:'get',
  })
}


