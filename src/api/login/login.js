import axios from '@/utils/request'

export function login(data) {
  return axios({
    url: '/admin/acl/index/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return axios({
    url: '/admin/acl/index/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return axios({
    url: '/admin/acl/index/logout',
    method: 'post'
  })
}
